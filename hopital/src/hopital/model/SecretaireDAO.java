package hopital.model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;


public class SecretaireDAO {
	
	/*public LinkedList<Patient> listePatient(Patient patient) throws ClassNotFoundException, SQLException{
			
		LinkedList<Patient> liste = new LinkedList<Patient>();
		
		liste.add(patient);
		
		if(patient ==null){
			System.out.println(liste);
			System.out.println("--------------------");
			for (Patient e : liste)
				System.out.println(e);
		}
		
		
		return liste;
	}
	*/
	
	// TT : Fichier Texte
	public void remplirFichierArrive(){
		
	}
	
	// 3 - Afficher le prochain.
	public Patient afficherProchain(LinkedList<Patient> patient){
		
			return patient.getLast();
			
	}
	
		// TT : Affiche la liste des patients 
	public ArrayList<Patient> afficherHistoriqueVisites() throws ClassNotFoundException, SQLException{
		
		Class.forName("com.mysql.jdbc.Driver");

		ArrayList<Patient> liste = new ArrayList<Patient>();
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/apside-apside", "root", "root");

		String sql = "SELECT * FROM visites";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while (rs.next()) {
			Patient p = new Patient();
			p.setId(rs.getString("id"));
			p.setNom(rs.getString("nom"));
			p.setPrenom(rs.getString("prenom"));
			p.setTelephone(rs.getString("telephone"));
			p.setAdresse(rs.getString("adresse"));
			liste.add(p);
		}
		conn.close();
		return liste;
		
	}
	
	// Inscrire patient dans la BDD
	public void inscrirePatient(Patient patient) throws SQLException, ClassNotFoundException{
		
		Class.forName("com.mysql.jdbc.Driver");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");


		String sql = "INSERT INTO patients values(?, ?, ?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, patient.getId());
		ps.setString(2, patient.getNom());
		ps.setString(3, patient.getPrenom());
		ps.setString(4, patient.getTelephone());
		ps.setString(5, patient.getAdresse());
		
		ps.executeUpdate();

		conn.close();
		System.out.println("Le patient a bien ete enregistre ! ");
	
	}
	
	
	public ArrayList<Patient> rechercherPatient(Patient patient) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		
		ArrayList<Patient> liste = new ArrayList<Patient>();

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");
		String sql = "SELECT * FROM patients WHERE nom like'" + patient.getNom() + "%'";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while (rs.next()) {
			 Patient p = new Patient();
			p.setId(rs.getString("id"));
			p.setNom(rs.getString("nom"));
			p.setPrenom(rs.getString("prenom"));
			p.setTelephone(rs.getString("telephone"));
			p.setAdresse(rs.getString("adresse"));
			
		}
		conn.close();
		return liste;
		
	}

	
	public boolean exist(String id) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Patient p = null;
		
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

		String sql = "SELECT * FROM patients WHERE id='"+id+"'";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		if (rs.next()) {
			p = new Patient(rs.getString("id"), 
					rs.getString("nom"),
					rs.getString("prenom"),
					rs.getString("telephone"), 
					rs.getString("adresse"));
			return true;
		}
		conn.close();
		return false;
	}
	
	public Patient selectPatient(String id) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		
		Patient p = null;

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");
		String sql = "SELECT * FROM patients WHERE id='"+id+"'";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while (rs.next()) {
			p = new Patient(rs.getString("id"), 
					rs.getString("nom"),
					rs.getString("prenom"),
					rs.getString("telephone"), 
					rs.getString("adresse"));
			
		}
		conn.close();
		return p;
		
	}
	
	
}
