package hopital.model;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//secretaire rempli une file d'attente 
//qd je cr�e un medecin je dois instancier une salle 

public class Medecin extends PersonnelHospitalier {

	List<Visite> visites;
	
	int numSalle;

	public void setVisites(List<Visite> visites) {
		this.visites = visites;
	}

	public Medecin(Authentification auth) {
		super(auth);
		
		this.numSalle = auth.getNumSalle();
		this.visites = new ArrayList<Visite>(

//				Arrays.asList(
//
//						new Visite( 1, "2022-11-23", this, this.numSalle),
//						new Visite( 2, "2022-11-23", this, this.numSalle),
//						new Visite( 3, "2022-11-23", this, this.numSalle),
//						new Visite( 4, "2022-11-23", this, this.numSalle)
//
//		)

		);

	}

	public List<Visite> getVisites() {
		return visites;
	}

	public Visite patientToVisit(Patient prochainPatient) {

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
		String strDate = dateFormat.format(date);

		return new Visite(Long.parseLong(prochainPatient.getId()), strDate, this, 1);

	}
	
	public void enregistrementAutomatiqueVisites(List<Visite> visites) {

		if (visites.size() >= 4)
			enregistrerVisite(visites, true);
		else
			return;

	}

	public void libererSalle() {

		
		List<Patient> fileDAttente = Hopital.getInstance().getFileDAttente();
		
		if(fileDAttente.size() == 0){
			
			System.out.println("Aucun patient en file d'attente.");
			return;
			
		}
		
		
		Patient prochainPatient = fileDAttente.remove(0);
	
		Visite nouvelleVisite = patientToVisit(prochainPatient);
		
		this.visites.add(nouvelleVisite);
		
		enregistrementAutomatiqueVisites(this.visites);
		
		System.out.println("visites non enregistr�es " + this.visites.size());
		
//		for (Visite v : this.visites)
//			System.out.println("\n"+v);


		Hopital.getInstance().setFileDAttente(fileDAttente);

	}

	public boolean enregistrerVisite(List<Visite> visites, boolean mode) {
		// insert sur la table visite ac idPatient et medcin

		VisiteDAO visitesDao = new VisiteDAO();

		try {
			visitesDao.insertVisits(this.visites);
			viderListeVisites();

		} catch (Exception e) {
			System.out.println(e);
		}

		return true;
	}

	public List<Visite> viderListeVisites() {

		this.setVisites(new ArrayList<Visite>());
		return this.getVisites();

	}

	public void afficherVisites(int medecinId) {

		VisiteDAO visitesDao = new VisiteDAO();

		try {
			List<String> visitList = visitesDao.selectByMedecinId(medecinId);

			System.out.println("NOM\tPRENOM\tTELEPHONE\t\tADRESSE\t\tDATE\tTARIF\n");

			for (String v : visitList)
				System.out.println(v);

		} catch (Exception e) {
			System.out.println(e);
		}

		System.out.println("");
	}

	@Override
	public String toString() {
		return "Medecin : " + super.toString();
	}

}
