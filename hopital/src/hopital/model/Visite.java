package hopital.model;

public class Visite {
	
	private int id;
	private long idPatient;
	private String date;
	private Medecin medecin;
	private int numSalle;
	private int tarif = 23;
	
	
	public Visite( long idPatient, String date, Medecin medecin, int numSalle) {
		
		this.idPatient = idPatient;
		this.date = date;
		this.medecin = medecin;
		this.numSalle = numSalle;
	}


	public int getNumSalle() {
		return numSalle;
	}


	public void setNumSalle(int numSalle) {
		this.numSalle = numSalle;
	}


	public int getTarif() {
		return tarif;
	}


	public void setTarif(int tarif) {
		this.tarif = tarif;
	}


	public int getId() {
		return id;
	}


	public long getIdPatient() {
		return idPatient;
	}


	public String getDate() {
		return date;
	}


	public Medecin getMedecin() {
		return medecin;
	}


	@Override
	public String toString() {
		return "Visite [id=" + id + ", idPatient=" + idPatient + ", date=" + date + ", medecin=" + medecin
				+ ", numSalle=" + numSalle + ", tarif=" + tarif + "]";
	}
	
	
	
	
	
	
	
	
	

}
