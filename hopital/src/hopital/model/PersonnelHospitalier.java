package hopital.model;

import java.util.ArrayList;
import java.util.List;

public abstract class PersonnelHospitalier {

	private Authentification auth;

	public Authentification getAuth() {
		return auth;
	}

	public PersonnelHospitalier(Authentification auth) {
		this.auth = auth;
	}

	@Override
	public String toString() {
		return "" + auth;
	}

	public void afficherListeDattente(List<Patient> liste) {

		if (liste.size() == 0) {

			System.out.println("Aucun patient en file d'attente.");
			return;

		}

		System.out.println("ID\t\tNOM\tPRENOM\tTELEPHONE\t\tADRESSE\n");
		for (Patient p : liste) {
			System.out.println(p.getId() + "\t" + p.getNom() + "\t" + p.getPrenom() + "\t" + p.getTelephone() + "\t"
					+ p.getAdresse());
		}

	}

	public boolean exit() {
		return true;
	}

}
