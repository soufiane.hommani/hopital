package hopital.user.modules;

import java.sql.SQLException;
import java.util.Scanner;

import hopital.model.Authentification;
import hopital.model.Hopital;
import hopital.model.Medecin;
import hopital.model.PersonnelHospitalier;
import hopital.user.GestionnairePatients;

public class MedecinModule {
	
	private Scanner sc = new Scanner(System.in);

	Medecin medecin;

	public MedecinModule(Authentification identifiedAuth) {
		this.medecin = new Medecin(identifiedAuth);
		afficheMenuMedecin();
	}
	
	public void separation(){
		
		System.out.println("\n");
		System.out.println("---------------------------------");
		
	}
	
	public void afficheMenuMedecin(){
		
		separation();
		System.out.println("Lancement du module Medecin ");
		System.out.println("choisissez une option : \n"
				+ "1. afficher la file d'attente \n"
				+ "2. lib�rer la salle \n"
				+ "3. enregistrer les visites en base \n"
				+ "4. afficher la liste de mes visites pass�es \n"
				+ "5. revenir au menu principal \n"
				+ "\n");
		
		int choix = this.sc.nextInt();
		
		//System.out.println("choix " + choix);
		
		this.appelChoix(choix);
		
	}
	
	public void appelChoix(int choix){
		switch (choix) {
		case 1:
			// appel afficher liste d'attente
			this.medecin.afficherListeDattente(Hopital.getInstance().getFileDAttente());
			afficheMenuMedecin();
			break;
		case 2:
			this.medecin.libererSalle();
			afficheMenuMedecin();
			break;
		case 3:
			this.medecin.enregistrerVisite(this.medecin.getVisites(), true);
			afficheMenuMedecin();
			break;
		case 4:
			this.medecin.afficherVisites(medecin.getAuth().getId());
			afficheMenuMedecin();
			break;
		case 5:
			separation();
			GestionnairePatients.lancementModule();
			break;
		default:
			break;
		}
	}
	
	

}
