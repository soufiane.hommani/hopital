package hopital.user.modules;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import hopital.model.Authentification;
import hopital.model.Hopital;
import hopital.model.Medecin;
import hopital.model.Patient;
import hopital.model.PatientDao;
import hopital.model.Secretaire;
import hopital.model.SecretaireDAO;
import hopital.model.VisiteDAO;
import hopital.user.GestionnairePatients;

public class SecretaireModule {
	
//	Hopital.getInstance().setFileDAttente();
	
	private Scanner sc = new Scanner(System.in);
	SecretaireDAO s1 = new SecretaireDAO();
	
	List<Patient> nouvelleFile = Hopital.getInstance().getFileDAttente();

	// LinkedList<Patient> listePatient = new LinkedList<Patient>();

	Secretaire secretaire;

	public SecretaireModule(Authentification identifiedAuth) throws ClassNotFoundException, SQLException {

		this.secretaire = new Secretaire(identifiedAuth);
		// System.out.println("Lancement module secr�taire" + secretaire);
		afficheMenuSecretaire();
	}

	public void afficheMenuSecretaire() throws ClassNotFoundException, SQLException {

		System.out.println("\nBienvenu dans l'espace secretaire \n");
		System.out.println("------------------------------------\n");

		System.out.println("Veuillez choisir une option : \n\n" + "1. ajouter un patient dans la file \n"
				+ "2. afficher la file d'attente \n" + "3. afficher le prochain patient  \n"
				+ "4. renseigner heure d'arriv�e patient\n" + "5. afficher historique des visites \n" + "6. sortir \n"
				+ "\n");

		int choix = this.sc.nextInt();

		this.appelChoix(choix);

	}

	public void appelChoix(int choix) throws ClassNotFoundException, SQLException {
		switch (choix) {
		case 1:
			ajoutFileAttente();
			afficheMenuSecretaire();
			break;
		case 2:
			this.secretaire.afficherListeDattente(nouvelleFile);
			afficheMenuSecretaire();
			break;
		case 3:
			afficherProchain(nouvelleFile);
			afficheMenuSecretaire();
			break;
		case 4:
			afficheMenuSecretaire();
			break;
		case 5:
			afficheHistoriqueVisite();
			afficheMenuSecretaire();
			break;
		case 6:
			GestionnairePatients.lancementModule();
			break;

		default:
			break;
		}
	}

	public void afficheHistoriqueVisite() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'id du patient : \n");

		long patientId = sc.nextLong();

		PatientDao pDao = new PatientDao();

		try {

			Patient pSelected = pDao.selectById(patientId);

			if (pSelected != null) {

				VisiteDAO visites = new VisiteDAO();

				List<String> historiqueDesVisites = visites.selectVisitsByPatientId(patientId);

				System.out.println("DATE\tIDMEDECIN\tIDSALLE\t\tTARIF\n");

				for (String s : historiqueDesVisites)
					System.out.println(s);

			}

			else {
				System.out.println("patient inconnu des systemes \n");
				return;

			}

		} catch (Exception e) {

			System.out.println(e);

		}

	}

	public void ajoutFileAttente() throws ClassNotFoundException, SQLException {

		Scanner sc1 = new Scanner(System.in);

		System.out.println("Veuillez saisir l'id : \n");

		String vi = sc1.next();

		if (s1.exist(vi)) {

			System.out.println("Le patient existe : " + s1.selectPatient(vi));
			System.out.println("\najout � la file d'attente");

			nouvelleFile.add(s1.selectPatient(vi));

			Hopital.getInstance().setFileDAttente(nouvelleFile);

		} else {
			System.out.println("Le patient n'existe pas : ");
			// System.out.println("Id : "+ p1.getId());
			System.out.println("Veuillez l'inscrire : ");

			Scanner sc = new Scanner(System.in);

			System.out.println("Veuillez saisir un ID (Numerique) : ");
			String id = sc.next();
			System.out.println("Veuillez saisir un Nom : ");
			String nom = sc.next();
			System.out.println("Veuillez saisir un prenom : ");
			String prenom = sc.next();
			System.out.println("Veuillez saisir un telephone : ");
			String telephone = sc.next();
			System.out.println("Veuillez saisir une adresse : ");
			String adresse = sc.next();

			Patient p = new Patient(id, nom, prenom, telephone, adresse);

			PatientDao pDao = new PatientDao();

			pDao.insert(p);

			// s1.inscrirePatient(p);

			nouvelleFile.add(p);

			Hopital.getInstance().setFileDAttente(nouvelleFile);
		}
	}

	public Patient afficherProchain(List<Patient> nouvelleFile) {

		System.out.println("Prochain patient : " + nouvelleFile.get(0));

		return nouvelleFile.get(0);

	}

	public void recherchePatient() throws ClassNotFoundException, SQLException {

		Patient p1 = new Patient("1", "Jeanne", "BBB", "0102030405", "AAA");
		Patient p2 = new Patient("1", "Jeanne", "BBB", "0102030405", "AAA");
		Patient p3 = new Patient("1", "Jeanne", "BBB", "0102030405", "AAA");

		SecretaireDAO s1 = new SecretaireDAO();

		System.out.println("s " + s1.rechercherPatient(p1));
	}

}
